<?php
  require_once('phpscripts/config.php');
  $ip = $_SERVER['REMOTE_ADDR'];// if asked, you can make sure that the only place where you can access the info is IN THE OFFICE
  // echo $ip;
  if(isset($_POST['submit'])){
    // echo "works";
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $email = trim($_POST['email']);
    $company = trim($_POST['company']);
    if($username !== "" && $password !== "" && $email !== "" && $company !== ""){ // == not identical to
      $result = logIn($username, $company, $password, $email, $ip);
      $message = $result;
    }else{
      $message = "Please fill out the required (ALL) fields";
    }
  }
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Admin Login - Digital London</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="icon" href="../img/icon/digital-london-logo-xs.png">
        <link rel="stylesheet" type="text/css" href="../css/foundation.min.css">
        <link rel="stylesheet" type="text/css" href="../css/main.css">
        <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
    </head>
    <body class="signinb">
        <!-- Header -->
        <header class="header">
            <div class="title-bar" data-responsive-toggle="example-animated-menu" data-hide-for="medium">
                <button class="menu-icon" type="button" data-toggle></button>
                <img src="../img/logo-small.png" class="menu__logo--small">
            </div>
            <div class="top-bar" id="example-animated-menu">
                <div class="top-bar-left">
                    <div class="logoDiv">
                        <a href="../index.php"><img src="../img/logo-small.png" class="menu__logo"></a>
                    </div>
                </div>
                <div class="top-bar-right">
                    <ul class="dropdown menu" data-dropdown-menu>
                        <li><a href="../index.php">Home</a></li>
                        <li><a href="#">Behind The Woods</a></li>
                        <li><a href="../testimonials.php">Testimonials</a></li>
                        <li><a href="../events.php">Events</a></li>
                        <li><a href="../jobs.php">Jobs</a></li>
                        <li><a href="../contact.php">Contact</a></li>
                        <li><a href="#">Sign In</a></li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="hero hero--contact"></div>
        <div class="container contact-con">
            <div class="cont">
                <div class="form">
                    <h3 class="form__message">TO POST <span>ADS, EVENTS / JOBS OR CREATE AN ACCOUNT</span> , SIGN IN WITH THE REGISTERED DETAILS.</h3>
                </div>
                <p>
                    <?php if(!empty($message)){ echo $message; } ?>
                </p>
                <form class="form" action="admin_login.php" method="post">
                    <div class="grid-x align-center">
                        <div class="medium-5  cell">
                            <input type="text" name="username" value="" placeholder="MEMBERSHIP NUMBER / USERNAME">
                            <input type="text" name="company" value="" placeholder="COMPANY NAME">
                            <input type="submit" name="submit" class="button" value="SIGN IN">
                        </div>
                        <div class="medium-5  cell medium-offset-1">
                            <input type="password" name="password" value="" placeholder="PASSWORD">
                            <input type="email" name="email" value="" placeholder="EMAIL">
                            <h3 class="form__message">Forgot your login details? Click <span style="border-bottom: 1px solid">here</span></h3>
                        </div>
                    </div>
                </form>
                <h3 class="form__message" data-aos="fade-up" data-aos-anchor-placement="top-bottom"><span><a href="admin_register.php">CLICK HERE</a> </span> IF YOU DON'T HAVE AN ACCOUNT.</h3>
            </div>
        </div>
        <footer class="footer">
            <div class="container-footer">
                <ul>
                    <li><a href="">Digital London</a></li>
                    <li><a href="">Careers</a></li>
                    <li><a href="">Privacy Policy</a></li>
                    <li><a href="">Disclaimer</a></li>
                    <li><a href="">Copyright</a></li>
                    <li><a href="">Sitemap</a></li>
                    <li><a class="ledc" href="https://www.ledc.com/">ledc</a></li>
                </ul>
                <div class="social">
                    <img src="../img/twitter.svg" alt="Twitter">
                    <img src="../img/facebook.svg" alt="facebook">
                    <img src="../img/instagram.svg" alt="Twitter">
                </div>
            </div>
        </footer>
        <script src="../js/vendor/jquery.js"></script>
        <script src="../js/vendor/what-input.js"></script>
        <script src="../js/vendor/foundation.js"></script>
        <script src="../js/app.js"></script>
        <!--<script src="js/signin.js"></script>-->
        <script src="../js/signinaos.js"></script>
        <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
    </body>
    </html>