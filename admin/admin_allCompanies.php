<?php
	require_once('phpscripts/config.php');
	confirm_logged_in();
	$tbl = "tbl_company";
	$getCompanies = getAll($tbl);
?>
    <!doctype html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>All Companies</title>
        <link rel="icon" href="../img/icon/gear-icon-xs.png">
        <link rel="stylesheet" href="../css/foundation.css" />
        <link rel="stylesheet" href="../css/cms-style.css" />
    </head>
    <body>
        <br>
        <div class="row">
            <div class="small-12 columns">
                <h2 class="centerText">All Companies</h2>
            </div>
            <div class="small-12 columns">
                <p class="centerText">Select which companies you would like to edit.</p>
            </div>
        </div>
        <br>
        <div class="row">
            <?php
	if(!is_string($getCompanies)){
		while($row = mysqli_fetch_array($getCompanies)){
			echo "<div class=\"small-12 medium-6 large-4 columns end\">
							<p class=\"all\">{$row['company_name']}</p>
							<a class=\"blackText\" href=\"edit/editJobs.php?id={$row['company_id']}\">Edit Company Details</a><br><br><br>
						</div>
			";
		}
	}else{
		echo "<p class=\"error\">{$getCompanies}</p>";
	}
?>
        </div>
        <script src="../js/vendor/jquery.min.js"></script>
        <script src="../js/vendor/what-input.min.js"></script>
        <script src="../js/foundation.min.js"></script>
        <script src="../js/app.js"></script>
    </body>
    </html>