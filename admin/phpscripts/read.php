<?php

	function getAll($tbl) {
		include('connect.php');
		$queryAll = "SELECT * FROM {$tbl}";
		$runAll = mysqli_query($link, $queryAll);
		// echo $queryAll;
		if($runAll){
				return $runAll;
		}else{
			$error = "An error has been found";
			return $error;
		}

		mysqli_close($link);
	}

	function getSingle($tbl, $col, $id) {
		include('connect.php');
		$querySingle = "SELECT * FROM {$tbl} WHERE {$col} = {$id}";
		// echo $querySingle;
		$runSingle = mysqli_query($link, $querySingle);

		if($runSingle){
			return $runSingle;
		}else{
			$error = "Something broke";
			return $error;
		}

		mysqli_close($link);
	}

	function filterResults($tbl, $tbl2, $tbl3, $col, $col2, $col3, $filter) {
			include('connect.php');
			$filterQuery = "SELECT * FROM {$tbl}, {$tbl2}, {$tbl3} WHERE {$tbl}.{$col} = {$tbl3}.{$col} AND {$tbl2}.{$col2} = {$tbl3}.{$col2} AND {$tbl2}.{$col3} = '{$filter}'";
			// echo $filterQuery;
			$runQuery = mysqli_query($link, $filterQuery);
			if($runQuery){
				return $runQuery;
			}else{
				$error = "There was a problem accessing this information. So sorry";
 			return $error;
		}
		mysqli_close($link);
	}

	function getOne($tbl, $col, $id1) {
		include('connect.php');
		$querySingle = "SELECT * FROM {$tbl} WHERE {$col} = {$id1}";
		// echo $querySingle;
		$getOne = mysqli_query($link, $querySingle);

		if($getOne){
			return $getOne;
		}else{
			$error = "Something has broken";
			return $error;
		}

		mysqli_close($link);
	}

	function getTwo($tbl, $col, $id2) {
		include('connect.php');
		$querySingle = "SELECT * FROM {$tbl} WHERE {$col} = {$id2}";
		// echo $querySingle;
		$getOne = mysqli_query($link, $querySingle);

		if($getOne){
			return $getOne;
		}else{
			$error = "Please try again later";
			return $error;
		}

		mysqli_close($link);
	}

	function getThree($tbl, $col, $id3) {
		include('connect.php');
		$querySingle = "SELECT * FROM {$tbl} WHERE {$col} = {$id3}";
		echo $querySingle;
		$getOne = mysqli_query($link, $querySingle);

		if($getOne){
			return $getOne;
		}else{
			$error = "ERROR: Come back again";
			return $error;
		}

		mysqli_close($link);
	}

function tryLL($cname){
	include('connect.php');
	$querySingle = "SELECT job_position FROM tbl_job j, tbl_company c, tbl_company_job cj WHERE c.company_id = cj.company_id AND j.job_id = cj.job_id AND c.company_name = '{$cname}'";
	// echo $querySingle;
	$getOne = mysqli_query($link, $querySingle);

	if($getOne){
		return $getOne;
	}else{
		$error = "ERROR: Come back again";
		return $error;
	}
	mysqli_close($link);
}

function pastevent($company, $tbla){
	include('connect.php');
	$query = "SELECT * FROM tbl_{$tbla} j, tbl_company c, tbl_company_{$tbla} cj WHERE c.company_id = cj.company_id AND j.{$tbla}_id = cj.{$tbla}_id AND c.company_name = '{$company}'";
	// echo $query;
	$getOne = mysqli_query($link, $query);

	if($getOne){
		return $getOne;
	}else{
		$error = "ERROR: Come back again";
		return $error;
	}
	mysqli_close($link);

}

?>
