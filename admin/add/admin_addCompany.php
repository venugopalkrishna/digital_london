<?php
	require_once('../phpscripts/config.php');
	confirm_logged_in();
	$tbl = "tbl_company";
	$genQuery = getAll($tbl);
	if(isset($_POST['submit'])){
		$name = $_POST['name'];
		$address = $_POST['address'];
		$compLink = $_POST['compLink'];
		$img = $_FILES['img'];
		$result = addCompany($name, $address, $compLink, $img);
		$message = $result;
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Add Company</title>
<link rel="icon" href="../../img/icon/gear-icon-xs.png">
<link rel="stylesheet" href="../../css/foundation.css"/>
<link rel="stylesheet" href="../../css/cms-style.css"/>
</head>
<body>
	<br>
		<div class="row">
			<div class="small-12 columns"><h2 class="centerText">Add Company</h2></div>
			<div class="small-12 columns"><p class="centerText">Add a new company to the database.</p></div>
		</div>
	<br>
	<?php if(!empty($message)){ echo $message;} ?>
	<form action="admin_addCompany.php" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="small-12 large-6 columns">
				<h3 class="hidden">Company Name</h3>
				<label>Company Name:</label>
				<input type="text" name="name" value="">
			</div>
		</div>
		<div class="row">
			<div class="small-12 large-6 columns">
				<h3 class="hidden">Company Address</h3>
				<label>Company Address:</label>
				<input type="text" name="address" value="">
				<br>
			</div>
		</div>
		<div class="row">
			<div class="small-12 large-6 columns">
				<h3 class="hidden">Company Website</h3>
				<label>Company Website:</label>
				<input type="text" name="compLink" value="">
				<br>
			</div>
		</div>
		<div class="row">
			<div class="small-12 large-2 columns">
				<h3 class="hidden">Company Image</h3>
				<label>Company Image:</label>
			</div>
			<div class="small-12 large-5 columns">
				<input type="file" name="img" value="">
			</div>
				<br>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="small-12 large-6 columns">
				<input type="submit" name="submit" value="Add Company">
			</div>
		</div>
	</form>
	</div>
	<br><br>
	<div class="row">
		<div class="small-12 columns"><a href="admin_index.php"><p class="blackText">BACK...</p></a></div>
	</div>
	<script src="../../js/vendor/jquery.js"></script>
	<script src="../../js/vendor/what-input.js"></script>
	<script src="../../js/vendor/foundation.min.js"></script>
	<script src="../../js/app.js"></script>
</body>
</html>