<?php
require_once('phpscripts/config.php');
  if(isset($_POST['submit'])) {
    $fname = trim($_POST['fname']);
    // $username = trim($_POST['username']);
    $fpassword = trim($_POST['fpass']);
    $password = trim($_POST['password']);
    $company = trim($_POST['company']);
    $phone = trim($_POST['phone']);
    $email = trim($_POST['email']);
    // $lvlist = trim($_POST['lvlist']);
    if($fpassword != $password) {
      $message = "Passwords don't match";
    }else{
      $gcript = criptpass($password);
      $result = createUser($fname, $email, $company, $phone, $password);
      $message= $result;
    }
  }
 ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Digital London</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="icon" href="../img/icon/gear-icon-xs.png">
        <link rel="stylesheet" type="text/css" href="../css/foundation.min.css">
        <link rel="stylesheet" type="text/css" href="../css/main.css">
        <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
        <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
    </head>
    <body class="signinb">
        <!-- Header -->
        <header class="header">
            <div class="title-bar" data-responsive-toggle="example-animated-menu" data-hide-for="medium">
                <button class="menu-icon" type="button" data-toggle></button>
                <img src="../img/logo-small.png" class="menu__logo--small">
            </div>
            <div class="top-bar" id="example-animated-menu">
                <div class="top-bar-left">
                    <div>
                        <img src="../img/logo-small.png" class="menu__logo">
                    </div>
                </div>
                <div class="top-bar-right">
                    <ul class="dropdown menu" data-dropdown-menu>
                        <li><a href="../home.php">Home</a></li>
                        <li><a href="../testimonials.php">Testimonials</a></li>
                        <li><a href="../events.php">Events</a></li>
                        <li><a href="../jobs.php">Jobs</a></li>
                        <li><a href="../contact.php">Contact</a></li>
                        <li><a href="../admin_login.php">Sign In</a></li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="hero hero--contact hero--signin"></div>
        <div class="container contact-con">
            <div class="cont">
                <div class="form">
                    <h3 class="form__message">TO POST <span>ADS, EVENTS / JOBS OR CREATE AN ACCOUNT</span> , REGISTER BELOW.</h3>
                </div>
                <p>
                    <?php if (!empty($message)){echo $message; } ?>
                </p>
                <form action="admin_register.php" method="post" class="form">
                    <div class="grid-x align-center">
                        <div class="medium-5  cell">
                            <input type="text" name="fname" placeholder="FULL NAME">
                            <input type="text" name="company" placeholder="COMPANY">
                            <input type="password" name="fpass" placeholder="PASSWORD">
                        </div>
                        <div class="medium-5  cell medium-offset-1">
                            <input type="number" name="phone" placeholder="PHONE NUMBER">
                            <input type="email" name="email" placeholder="EMAIL">
                            <input type="password" name="password" placeholder="CONFIRM PASSWORD">
                        </div>
                        <div class="medium-11 cell ">
                            <input type="submit" name="submit" class="button" value="REGISTER">
                        </div>
                    </div>
                </form>
                <h3 class="form__message" data-aos="fade-up" data-aos-anchor-placement="top-bottom"><span><a href="../admin_login.php">CLICK HERE</a> </span> IF YOU ARE ALREDY A MEMBER.</h3>
            </div>
        </div>
        <footer class="footer">
            <div class="container-footer">
                <ul>
                    <li><a href="">Digital London</a></li>
                    <li><a href="">Careers</a></li>
                    <li><a href="">Privacy Policy</a></li>
                    <li><a href="">Disclaimer</a></li>
                    <li><a href="">Copyright</a></li>
                    <li><a href="">Sitemap</a></li>
                    <li><a class="ledc" href="https://www.ledc.com/">Ledc</a></li>
                </ul>
                <div class="social">
                    <img src="../img/twitter.svg" alt="Twitter">
                    <img src="../img/facebook.svg" alt="facebook">
                    <img src="../img/instagram.svg" alt="Twitter">
                </div>
            </div>
        </footer>
        <script src="../js/vendor/jquery.js"></script>
        <script src="../js/vendor/what-input.js"></script>
        <script src="../js/vendor/foundation.js"></script>
        <script src="../js/app.js"></script>
        <script src="js/signin.js"></script>
        <script src="../js/signinaos.js"></script>

        <script type="text/javascript">
        var scrollitem = document.querySelector(".container").offsetTop;
        var hero = document.querySelector(".hero");

        window.onscroll = function() {
          console.log("works?");
          if (window.pageYOffset > 0) {
         var opac = (window.pageYOffset / scrollitem);
            console.log(opac);
          hero.style.background = "linear-gradient(rgba(255, 255, 255, " + opac + "), rgba(255, 255, 255, " + opac + ")), url('../img/banner_signin.jpg') no-repeat fixed";
          }
        }
        </script>
    </body>
    </html>
