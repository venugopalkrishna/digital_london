<?php include('phpscripts/config.php');
	$tbl = "tbl_testimonies";
	$count = 0;
	$getValues = getAll($tbl);
	// confirm_logged_in();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>All Testimonials</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" href="../img/icon/gear-icon-xs.png">
    <link rel="stylesheet" type="text/css" href="../css/foundation.css">
    <link rel="stylesheet" type="text/css" href="../css/cms-style.css">
</head>
<body>
    <div id="container">
        <br>
        <div class="row">
            <div class="small-12 columns">
                <h2 class="centerText">Edit Testimonials</h2>
            </div>
        </div>
        <br>
        <div class="row">
            <?php
			while($row = mysqli_fetch_array($getValues)){
			if($count === 0){
				echo "<div class=\"small-12 medium-6 large-3 columns\">
								<p><span class=\"bold\">Name;</span> {$row['test_name']}</p>
								<p><span class=\"bold\">Company;</span> {$row['test_company']}</p>
								<p><span class=\"bold\">Occupation;</span> {$row['test_title']}</p>
								<a href=\"edit/editTestimonies.php?id={$row['test_id']}\"><p> Edit {$row['test_name']}</p></a>
								<br><br>
							</div>";
							$count++;
			}else{
				echo "<div class=\"small-12 medium-6 large-3 columns\">
								<p><span class=\"bold\">Name;</span> {$row['test_name']}</p>
								<p><span class=\"bold\">Company;</span> {$row['test_company']}</p>
								<p><span class=\"bold\">Occupation;</span> {$row['test_title']}</p>
								<a href=\"edit/editTestimonies.php?id={$row['test_id']}\"><p> Edit {$row['test_name']}</p></a>
								<br><br>
							</div>";
							$count = 0;
				}
			}
		?>
        </div>
    </div>
    <script src="../js/testimonials.js"></script>
    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/vendor/what-input.js"></script>
    <script src="../js/vendor/foundation.js"></script>
    <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
    <script src="../js/app.js"></script>
</body>
</html>
