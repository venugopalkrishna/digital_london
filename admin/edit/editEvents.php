<?php
	require_once('../phpscripts/config.php');
	if(isset($_GET['id'])){
		$id = $_GET['id'];
	}
	confirm_logged_in();
?>
    <!doctype html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>Edit Testimony</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="icon" href="../../img/icon/gear-icon-xs.png">
        <link rel="stylesheet" type="text/css" href="../../css/foundation.css">
        <link rel="stylesheet" type="text/css" href="../../css/cms-style.css">
    </head>
    <body>
        <br>
        <div class="row">
            <div class="small-12 columns">
                <h2 class="centerText">Edit Events</h2>
            </div>
            <div class="small-12 columns">
                <p class="centerText">Edit the details of this event.</p>
            </div>
        </div>
        <div class="row">
            <div class="small-12 large-6 columns">
                <?php
				$tbl = "tbl_event";
				$col = "event_id";
				$id = $_GET['id'];
				$name = "event";
				echo single_edit($tbl, $col, $id, $name);
			?>
            </div>
        </div>
    </body>
    </html>
