<?php
	include('../phpscripts/config.php');
	confirm_logged_in();
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Edit Index Details</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" href="../../img/icon/gear-icon-xs.png">
    <link rel="stylesheet" type="text/css" href="../../css/foundation.css">
    <link rel="stylesheet" type="text/css" href="../../css/cms-style.css">
</head>
<body>
    <br>
    <div class="row">
        <div class="small-12 columns">
            <h2 class="centerText">Edit Landing Page Content</h2>
        </div>
        <div class="small-12 columns">
            <p class="centerText">Edit the details of the Landing Page.</p>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <?php
				$tbl = "tbl_index";
				$col = "index_id";
				$id = 1;
				$name = "index";
				// echo "okay";
				echo single_edit($tbl, $col, $id, $name);
			?>
        </div>
    </div>
</body>
</html>
