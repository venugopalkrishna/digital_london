-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 15, 2018 at 06:53 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digital_london`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_clicker`
--

DROP TABLE IF EXISTS `tbl_clicker`;
CREATE TABLE IF NOT EXISTS `tbl_clicker` (
  `click_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `click_button` varchar(50) NOT NULL,
  `click_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`click_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_clicker`
--

INSERT INTO `tbl_clicker` (`click_id`, `click_button`, `click_date`) VALUES
(1, 'btn1', '2018-02-25 06:07:04'),
(2, 'btn2', '2018-02-25 06:12:12'),
(3, 'btn3', '2018-02-25 06:12:14'),
(4, 'btn1', '2018-02-25 06:12:15'),
(5, 'btn2', '2018-02-25 06:12:16'),
(6, 'btn1', '2018-02-25 06:12:16'),
(7, 'btn3', '2018-02-25 06:12:17'),
(8, 'btn3', '2018-02-25 06:12:39'),
(9, 'btn3', '2018-02-25 06:13:32'),
(10, 'btn1', '2018-02-25 06:13:43'),
(11, 'btn2', '2018-02-25 06:13:51'),
(12, 'btn2', '2018-02-25 06:14:18'),
(13, 'btn1', '2018-02-25 06:14:20'),
(14, 'btn3', '2018-02-25 06:14:27'),
(15, 'btn2', '2018-02-25 06:14:33');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company`
--

DROP TABLE IF EXISTS `tbl_company`;
CREATE TABLE IF NOT EXISTS `tbl_company` (
  `company_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_name` varchar(200) NOT NULL,
  `company_address` varchar(180) NOT NULL,
  `company_phone` varchar(30) NOT NULL,
  `company_link` varchar(200) NOT NULL,
  `company_image` varchar(60) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_company`
--

INSERT INTO `tbl_company` (`company_id`, `company_name`, `company_address`, `company_phone`, `company_link`, `company_image`) VALUES
(1, 'Northern', '300 Wellington Street Unit 200, N6B 2L5', '+1 866-930-4608', 'https://www.northern.co/', 'northern.png'),
(2, 'Arcane', '304 Talbot Street London, Ontario - N6A 2R4', '(226) 289-2445', 'http://arcane.ws/', 'arcane.png'),
(3, 'Diply', '383 Richmond Street London, Ontario - N6A 3C4', '519-850-1991', 'https://diply.com/', 'diply.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_event`
--

DROP TABLE IF EXISTS `tbl_company_event`;
CREATE TABLE IF NOT EXISTS `tbl_company_event` (
  `ce_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` smallint(6) NOT NULL,
  `event_id` smallint(6) NOT NULL,
  PRIMARY KEY (`ce_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_company_event`
--

INSERT INTO `tbl_company_event` (`ce_id`, `company_id`, `event_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(5, 1, 16),
(6, 1, 17),
(7, 1, 18),
(8, 3, 21);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_job`
--

DROP TABLE IF EXISTS `tbl_company_job`;
CREATE TABLE IF NOT EXISTS `tbl_company_job` (
  `cj_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` smallint(6) NOT NULL,
  `job_id` smallint(6) NOT NULL,
  PRIMARY KEY (`cj_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_company_job`
--

INSERT INTO `tbl_company_job` (`cj_id`, `company_id`, `job_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 6),
(7, 2, 7),
(8, 3, 8),
(9, 3, 9);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event`
--

DROP TABLE IF EXISTS `tbl_event`;
CREATE TABLE IF NOT EXISTS `tbl_event` (
  `event_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `event_name` varchar(100) NOT NULL,
  `event_date` varchar(40) NOT NULL,
  `event_location` varchar(150) NOT NULL,
  `event_description` text NOT NULL,
  `event_img` varchar(50) DEFAULT NULL,
  `event_link` text NOT NULL,
  `event_duration` varchar(80) NOT NULL,
  `event_type` varchar(30) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_event`
--

INSERT INTO `tbl_event` (`event_id`, `event_name`, `event_date`, `event_location`, `event_description`, `event_img`, `event_link`, `event_duration`, `event_type`) VALUES
(1, 'TENNO CON 2018', 'JULY 7', 'London Convention Centre', 'The first five years were just the beginning. See the future of Warframe! Exciting reveals, guests, and an opportunity to ask the devs your questions live and see them in person. You won\'t want to miss this!', 'event1.png', 'https://www.eventbrite.com/e/tennocon-2018-tickets-43263090067', '1', 'tech'),
(2, 'UX Talks 003', 'APRIL 17', 'Wolf Performance Hall', 'UX Talks is a two-hour mini-conference covering all that is UX. This free event is great for students, designers and developers of all experience levels, business leaders, marketing and communications professionals, product people, and anyone looking to learn more about user experience.', 'event2.png', 'https://www.eventbrite.ca/e/ux-talks-003-tickets-43894532729', '1', 'tech'),
(3, 'TMT Predictions 2018', 'May 9', 'Toboggan Brewing Company', 'Anticipate the future. Strategize and adapt. Thrive. Deloitte\'s Technology, Media and Telecommunications Predictions are back! Learn which disruptions will be game-changers within the next 12 months.', 'event3.png', 'https://www.eventbrite.ca/e/deloittes-tmt-predictions-2018-the-future-in-technology-media-telecommunications-tickets-44154739013', '1', 'tech'),
(18, 'Cirque du Soleil Crystal', 'JUN 14 - 17', 'Budweiser Gardens', 'Cirque du Soleil is coming to London and Hamilton with its 42nd creation and first ever on ice experience. Cirque du Soleil&rsquo;s CRYSTAL, explores the artistic limits of ice for the first time in the company&rsquo;s 34-year history.', 'woods_event1.jpg', 'https://www.cirquedusoleil.com/', '2018-12-30', 'tour'),
(19, 'The Great Canadian Caesarfest', 'APRIL 28', '333 Richmond St. London, ON (Former G.T.\'s building) 12pm to 8pm', 'This is a one-of-a-kind event; showcasing everything Caesar Cocktail and features live music and Caesar blending competitions. Sign Up by email: info@greatcanadiancaesarfest.com.', 'woods_event2.jpg', 'https://www.londontourism.ca/Events', '2018-12-31', 'tour'),
(20, 'Ontario Summer Games', 'August 3 - 5 ', 'City of London', 'The Ontario Summer Games are the largest multi-sport event in the province, with over 3,000 youth athletes, coaches, managers, and officials competing in 22-27 different sports. Hosting the event is expected to produce an economic impact of over $6 million dollars for the City of London..', 'woods_event3.jpg', 'http://www.london2018.ca/', '2 days', 'tour');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_header`
--

DROP TABLE IF EXISTS `tbl_header`;
CREATE TABLE IF NOT EXISTS `tbl_header` (
  `header_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `header_name` varchar(50) NOT NULL,
  PRIMARY KEY (`header_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_index`
--

DROP TABLE IF EXISTS `tbl_index`;
CREATE TABLE IF NOT EXISTS `tbl_index` (
  `index_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `index_content` varchar(3000) NOT NULL,
  `index_content2` varchar(2000) NOT NULL,
  `index_video` varchar(100) NOT NULL,
  PRIMARY KEY (`index_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_index`
--

INSERT INTO `tbl_index` (`index_id`, `index_content`, `index_content2`, `index_video`) VALUES
(1, '\r\n\r\nWelcome to Digital London, Canada. Home to a thriving tech industry and digital creative sector, with a diverse range of careers and opportunities. Shaping the world of the future together.		', 'London brings an excellent quality of life, a good family oriented community, strong cultural events, and opportunities to shape the city while contributing to global Digital Creative and Tech conversations and innovations. 				<br><br> London also has a supportive and collaborative network of agencies, educators and professionals who are collectively pushing for a brighter future with a digital landscape.\r\n', 'dl_long.mp4');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job`
--

DROP TABLE IF EXISTS `tbl_job`;
CREATE TABLE IF NOT EXISTS `tbl_job` (
  `job_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_position` varchar(120) NOT NULL,
  `job_date` varchar(50) NOT NULL,
  `job_duration` varchar(150) NOT NULL,
  `job_description` text NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_job`
--

INSERT INTO `tbl_job` (`job_id`, `job_position`, `job_date`, `job_duration`, `job_description`) VALUES
(1, 'Senior Web Developer', 'April 1, 2018', '2 days', 'Fullscript is a health tech e-commerce startup that has created a platform for healthcare professions to dispense professional-grade nutritional supplements to their patients. Healthcare professionals prescribe the products, and we ship it to their patients. We are the market leaders, have grown over 2000% in 3 years and we go above and beyond to make dispensing wellness easy.'),
(2, 'E-commerce Developer', 'April 1, 2018', '2 days', 'Working from our downtown office, you will report to the Fulfillment Lead and will assist in architecting, building, improving, and maintaining world-class solutions for the preparation and delivery of customer orders. This is a hands-on role in the E-Commerce Development Team where you will develop software across the full-stack of a multi-channel platform from HTML, CSS &amp; Javascript to server-side Java, native iOS, and application specific database tuning on Oracle. '),
(3, 'Senior Graphic Developer', 'April 11, 2018', '5 days', 'This creative and innovative office is seeking an Online Graphic Designer to create works of graphic design and provide front-end support for a variety of electronic-based publications. Publications will reach a vastly diverse audience and will range from websites to mobile applications. Special emphasis is placed on visually communicating complex ideas in clear and concise ways, helping people to understand and be engaged by astronomy and the sciences.'),
(4, 'Account/Project Manager', 'April 12, 2018', '6 days', 'In your role as Project Administrator your main goal is to combine your stellar customer service experience with your drive to generate recurring business, resulting in happy, loyal clients who will continue to work with Voices.com again and again. You will report to the Sales Manager and will be integral part of the unbelievably awesome Professional Services team. '),
(5, 'SUMMER 2018 INTERNSHIP IN GRAPHIC DESIGN, WEB DEVELOPING, ETC.', 'March 27, 2018', '10 days', 'Position requires sound comprehensive knowledge of design and drafting techniques, engineering drawing, CAD/graphics, and reading of drawings. Responsible for developing concepts, completing designs, structures and plant arrangements while working both independently and as a part of a team. Good knowledge of industry and regulatory standards and codes is preferred.'),
(6, 'Digital Marketing Specialist, Intermediate', 'March 14, 2018', '20 days', 'Fairstone is looking for a Digital Marketing Specialist to add to our growing digital marketing team. The Digital Marketing Strategist will primarily work with and assist the Digital Marketing Manager in managing the company&rsquo;s robust digital acquisition channels, diversifying the sources of online business over time, and contributing to Fairstone&rsquo;s social media management with the Social Media Manager. '),
(7, 'SEO Specialist', 'April 13, 2018', '5 days', 'The candidate will be working intimately with the head of marketing on implementing a comprehensive email, CRM, SEM, and branding marketing strategy. In addition, you will then be tasked with a wide variety of projects and be given the autonomy to manage these projects with strategic oversight and support from the team. The successful candidate will be valued as (and expected to be) a contributing team member from day one.'),
(8, 'Full-stack Developer (C # and) JavaScript', 'April 7, 2018', '13 days', 'To continue the build of our text messaging platform and mission-critical applications. While maintaining our existing software will be crucial, further architecting and development of a multi-tenant platform will be paramount. TapOnIt is looking for a strong developer with leadership qualities as well as an ability to leverage new technology opportunities to keep us on the leading edge of mobile technologies. This position should grow into one that will manage supporting engineers.'),
(9, 'Project Manager: Development', 'April 14, 2018', '30 days', 'As a Junior Development Manager, the successful incumbent will be involved in all aspects of the real estate development process, including the creation of budgets; preparation of proformas, coordination of the entitlement process; confirming the municipal approvals are processed; working directly with consultants; creating and overseeing schedules; keeping projects on track; ensuring all legal agreements are completed; working with surveyors on strata plans, and filing disclosure statements. You will also be collaborating and debating with different departments in positioning the building for a specific target. ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testimonies`
--

DROP TABLE IF EXISTS `tbl_testimonies`;
CREATE TABLE IF NOT EXISTS `tbl_testimonies` (
  `test_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `test_name` varchar(200) NOT NULL,
  `test_title` varchar(200) NOT NULL,
  `test_company` varchar(250) NOT NULL,
  `test_description` text NOT NULL,
  `test_image` varchar(70) NOT NULL,
  `test_video` varchar(100) NOT NULL,
  PRIMARY KEY (`test_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_testimonies`
--

INSERT INTO `tbl_testimonies` (`test_id`, `test_name`, `test_title`, `test_company`, `test_description`, `test_image`, `test_video`) VALUES
(1, 'Diana Caballero3', 'UX Research, Design &amp; Consulting', 'Designer, ResIm', 'One of the benefits of being a young professional in a developing city like London is that we have the choice and the opportunity to shape the place we live in.', 'Diana.jpg', 'DigitalLondon_Diana.mp4'),
(2, 'Titus Ferguson', 'Media Association', 'Executive Director, UnLondon Digital', 'There\'s everything we need here in London to be successful, but we still remain a very well-kept secret and that\'s what we\'re trying to change. Letting more people know about the great things that go on here.', 'Titus.jpg', 'DigitalLondon_Titus.mp4'),
(3, 'Breck Campbell', 'Creative Director\r\n', 'Breck Campbell Design', 'For somebody like myself... partway through my career with young children, London\'s a really great place to be.', 'Breck.jpg', 'DigitalLondon_Breck.mp4'),
(4, 'Melissa Sariffodeen', 'CEO, Canada Learning Code', 'Canada Learning Code', 'When we think about London and the thriving community that\'s already here for creative technologists and tech industry professionals, it\'s actually really remarkable just how vibrant this community is, for this city and the country as well.', 'Melissa.jpg', 'DigitalLondon_Melissa.mp4\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `user_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_fname` varchar(250) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `user_pass` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_phone` varchar(15) NOT NULL,
  `user_company` varchar(150) NOT NULL,
  `user_date` varchar(70) DEFAULT NULL,
  `user_ip` varchar(50) NOT NULL DEFAULT 'no',
  `user_attempts` tinyint(4) NOT NULL,
  `user_lvl` varchar(50) DEFAULT NULL,
  `user_log` smallint(6) DEFAULT NULL,
  `user_newt` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `user_fname`, `user_name`, `user_pass`, `user_email`, `user_phone`, `user_company`, `user_date`, `user_ip`, `user_attempts`, `user_lvl`, `user_log`, `user_newt`) VALUES
(1, 'pietro', 'awe', 'lol', 'holo@gol', '453545', 'guat', NULL, 'no', 1, NULL, NULL, NULL),
(11, 'Soumya', 'changeplis', 'qF3oEoNjsPZ0::db9353854039d9da2b106fda41f286a0', 'soumya', '123', 's@123.com', NULL, 'no', 0, NULL, 4, '2018-03-22 13:51:21'),
(12, 'abc', 'changeplis', 'QwrwXFOl::571766c4c84e5d0cb703526fbfa9591e', 'abc@abc.com', '123456789', 'abc', NULL, 'no', 0, NULL, 4, '2018-03-22 14:08:54'),
(13, 'Chi Mai', 'deafult', 'EJrdpNYI::a9435d1aa50c7e1757e3b067329e0271', 'abc@bc.com', '123456789', 'abc', NULL, 'no', 1, '1', NULL, '2018-03-23 15:03:34'),
(14, 'Goliath', 'default', 'xbrL::9cdaff81f4583d08faf0a9f608f0eb81', 'goli@lol', '999999', 'Northern', '2018-04-11 13:55:14', '::1', 1, '0', 1, '2018-04-11 13:51:42'),
(15, 'Potato', 'potato', 'YiLv::5b73b7d9f4ea7323e34057621cb50edd', 'pot@lol', '8989898989', 'Northern', '2018-04-13 10:24:16', '::1', 0, '0', 1, '2018-04-13 10:22:59'),
(16, 'Luka', 'luka', 'hI/1::2505da6f2fc63dc799813799c8a1f846', 'luka@lol', '456486543', 'Arcane', '2018-04-13 17:47:45', '::1', 0, '0', 1, '2018-04-13 17:46:51'),
(17, 'Meiro', 'meir', 'lXU0::22718e4d468bcf556b3e481bd7f10afb', 'meiro@lol', '45454545', 'Arcane', NULL, 'no', 0, '0', NULL, '2018-04-13 18:08:01'),
(18, 'Abesto', 'asd', 'ikHb::5f1a4610196eb4d44465fc8b080204cd', 'asd@asd', '454545', 'Arcane', NULL, 'no', 0, '0', NULL, '2018-04-13 18:10:06'),
(20, 'Coco', 'coco', '7G5+::ed4382d0ca7ae5a1c2b5a55143333d1f', 'coco@lol', '753789', 'Diply', '2018-04-13 18:25:50', '::1', 0, '0', 1, '2018-04-13 18:24:49'),
(21, 'Maria', 'mari', '6+vb::061bc48b30a2b68596b2fc02afaa3489', 'mar@lol', '753214', 'Diply', NULL, 'no', 0, '0', NULL, '2018-04-13 18:37:19'),
(22, 'Bell', 'bell', '/ehk::95ee62c8372f33e2d1b3b2db823248da', 'bell@lol', '753698', 'Diply', '2018-04-14 14:12:15', '::1', 0, '0', 5, '2018-04-13 18:50:20'),
(23, 'admin', 'admin', '4JKNJw==::5e3bcf16c165f69702c87bdf822fb842', 'admin@ledc', '789456123', 'ledc', '2018-04-14 23:07:30', '::1', 0, '1', 3, '2018-04-13 23:38:25');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_woods`
--

DROP TABLE IF EXISTS `tbl_woods`;
CREATE TABLE IF NOT EXISTS `tbl_woods` (
  `woods_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `woods_title` varchar(200) DEFAULT NULL,
  `woods_name` varchar(200) DEFAULT NULL,
  `woods_location` varchar(250) DEFAULT NULL,
  `woods_description` text NOT NULL,
  `woods_image` varchar(70) DEFAULT NULL,
  `woods_video` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`woods_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_woods`
--

INSERT INTO `tbl_woods` (`woods_id`, `woods_title`, `woods_name`, `woods_location`, `woods_description`, `woods_image`, `woods_video`) VALUES
(1, NULL, NULL, NULL, 'Parks &amp; Outdoor Activities - Major parks such Victoria Park located in the heart of London often hold festival events encouraging communities to come and enjoy. Breathtaking landscape and family-friendly camping support let London be a diverse city for many healthy activities.', 'woods1.jpg', NULL),
(2, 'The Birth of Insulin', 'Banting House of Dr. Frederick Banting', '442 Adelaide St N, London, ON N6B 3H8', 'The fight against diabete started here.A curious tour to a historical heritage left by Dr. Frederick Banting (1891 - 1941), where he researched and discovered insulin as a treatment to diabetes.', 'woods2.jpg', NULL),
(3, NULL, NULL, NULL, 'Museums are for Cool Kids! - A high number of museums exhibiting wide range of collections that serve the curious minds and eyes. Come to www.museumlondon.ca or tourismlondon.ca for more information.', 'woods3.jpg', NULL),
(4, NULL, 'The Headquarters of Labatt Beer Brewery', '150 Simcoe St, London, ON N6A 4M3', 'Labatt Brewery Tours run all year round...and MUST be pre-booked by email (londontour@labatt.com) or by phone at 519-850-TOUR(8687). The tour takes about 2 hours to complete and includes a full guided tour of the brewery, and samplings. The tour costs $12 per person (cash only) and everyone on the tour MUST wear closed toe, closed heel and flat footwear (running shoes preferred).', 'woods4.jpg', NULL),
(5, NULL, NULL, NULL, '\'My City\' by Holly Painter Poetry - An authentic portrait revealing the community of London with content shared by the community.', 'woods5.jpg', 'https://www.youtube.com/watch?v=hJUhSSMjISc'),
(6, NULL, NULL, NULL, '\'Dundas Street\' featured by Downtown London #GetDTL - Follow Istiana on a fun tour of Dundas downtown during summer celebrations. Music by Texas King, Video by Lilac Media Group.', 'woods6.jpg', 'https://www.youtube.com/watch?v=hucINiEspZs');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
