<?php
	ini_set('display_errors',1);
	error_reporting(E_ALL);
	require_once('phpscripts/config.php');
	// confirm_logged_in();
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Admin Panel</title>
    <link rel="icon" href="../img/icon/gear-icon-xs.png">
    <link rel="stylesheet" href="../css/foundation.css" />
    <link rel="stylesheet" href="../css/cms-style.css" />
</head>
<body>
    <div>
        <br>
        <div class="row">
            <div class="small-12 columns">
                <h2 class="centerText">Welcome Back!</h2>
            </div>
            <div class="small-12 columns">
                <p class="centerText">You are logged in to the admin page!</p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="small-12 large-4 columns end">
                <h3 class="hidden">Edit Index Content</h3>
                <a href="edit/editIndex.php">
                    <p class="centerText">Edit Index Content</p>
                </a>
            </div>
            <div class="small-12 large-4 columns end">
                <h3 class="hidden">All Testimonies</h3>
                <a href="admin_testimonials.php">
                    <p class="centerText">All Testimonies</p>
                </a>
            </div>
            <div class="small-12 large-4 columns end">
                <h3 class="hidden">All Jobs</h3>
                <a href="admin_allJobs.php">
                    <p class="centerText">All Jobs</p>
                </a>
            </div>
            <div class="small-12 large-4 columns end">
                <h3 class="hidden">All Events</h3>
                <a href="admin_allEvents.php">
                    <p class="centerText">All Events</p>
                </a>
            </div>
            <div class="small-12 large-4 columns end">
                <h3 class="hidden">All Companies</h3>
                <a href="admin_allCompanies.php">
                    <p class="centerText">All Companies</p>
                </a>
            </div>
            <div class="small-12 large-4 columns end">
                <h3 class="hidden">Sign Out</h3>
                <a href="phpscripts/caller.php?caller_id=logout">
                    <p class="centerText">Sign Out</p>
                </a>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="small-12 large-4 columns end">
                <h3 class="hidden">Add New Job/Event</h3>
                <a href="../new_post.php">
                    <p class="centerText">Add New Job / Event</p>
                </a>
            </div>
            <div class="small-12 large-4 columns end">
                <h3 class="hidden">Add New Company</h3>
                <a href="add/admin_addCompany.php">
                    <p class="centerText">Add New Company</p>
                </a>
            </div>
        </div>
    </div>
    <script src="../js/vendor/jquery.min.js"></script>
    <script src="../js/vendor/what-input.min.js"></script>
    <script src="../js/foundation.min.js"></script>
    <script src="../js/app.js"></script>
</body>
</html>
