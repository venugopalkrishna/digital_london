<?php
require_once('admin/phpscripts/config.php');
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Digital London</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/events.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

	<!-- Header -->
<?php include "includes/menu.php" ?>
	<div style="background: url('img/banner_jobs.png') no-repeat;" class="hero hero--job">

	</div>

	<div class="container container-login jobs">
		<div class="form">
			<br>
			<!-- <div class="grid-x align-between company__tags">

				<a href="profile.html" class="cell medium-3">COMPANY PROFILE</a>


				<a href="past_events.html" class="cell medium-3">PAST EVENTS</a>


				<a href="past_jobs.html" class="cell medium-3">PAST JOBS</a>


				<a href="#" class="cell medium-3 ">NEW JOBS/EVENTS</a>

			</div> -->

		</div>
		<br>

		<div class="grid-x container__company">

			<?php
			$tbl = "tbl_company";
			$getinfo = getAll($tbl);
			// $getjobs = tryLL();
			$classes =  ["cdiv", "cdiv2", "cdiv3"];
			$classjob = ["job_border", "job_border2", "job_border3"];
			$count = 0;
			while($row = mysqli_fetch_array($getinfo)) {
				if($count == 1){
					echo "<div class=\"cell medium-4 companyborder {$classes[$count]}\">";
				}else{
					echo "<div class=\"cell medium-4 {$classes[$count]}\">";
				}
					echo "<img src=\"img/{$row['company_image']}\" alt=\"{$row['company_name']}\">
					<h4>{$row['company_name']}</h4>
					<h5 class=\"{$classjob[$count]}\">{$row['company_address']}</h5>";
					$getjobs = tryLL($row['company_name']);
					while($row = mysqli_fetch_array($getjobs)) {
						echo "<p class=\"{$classjob[$count]}\">{$row['job_position']}</p>";
					}
				echo "<a href=\"{$row['company_link']}\"><button class=\"button_jobs\">APPLY NOW!</button></a>
				</div>";
				if ($count == 3){
					$count = 0;
				}else{
					$count++;
				}
			}

			 ?>
		</div>

	</div>

  <?php include "includes/footer.php" ?>

	<script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
    <script type="text/javascript">

    var scrollitem = document.querySelector(".jobs").offsetTop;
    var hero = document.querySelector(".hero");

    window.onscroll = function() {
      if (window.pageYOffset > 0) {
     var opac = (window.pageYOffset / scrollitem);
        console.log(opac);
      hero.style.background = "linear-gradient(rgba(255, 255, 255, " + opac + "), rgba(255, 255, 255, " + opac + ")), url('img/banner_jobs.png') no-repeat fixed";
      }
    }

    </script>

</body>
</html>
