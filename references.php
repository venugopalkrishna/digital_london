<?php include('admin/phpscripts/config.php');
$tbl = "tbl_index";

$getValues = getAll($tbl);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Digital London</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
</head>
<body class="home">

	<!-- Header -->
	<?php include "includes/menu.php"; ?>

	<div class="hero">

	</div>

	<div class="data__company">
		<!--stats div-->
		<div class="data grid-x">
      <h2 style="color: white;">REFERENCES</h2>
		</div>
  </div>

		<div class="description">
			<div class="container">
				<div class="grid-x">

					<!--video div-->

          <div class="small-12 medium-4 ">
								<ul>
                  <li><a href="https://www.ledc.com/">LEDC</a></li>
                  <li><a href="https://res.im/">ResIm</a></li>
                  <li><a href="https://twitter.com/LadiesThatUXLDT">Ladies that UX London<a></li>
                  <li><a href="https://www.canadalearningcode.ca/">Canada Learning Code</a></li>
                  <li><a href="https://www.tedxyouthtoronto.ca/melissa">Melissa’s Tedx-Youth Talk</a></li>
                  <li><a href="http://www.unlondon.ca/">UnLondon</a></li>
                  <li><a href="http://breckcampbell.design/">Breck Campbell Design</a></li>
                  <li><a href="https://www.innovationworkslondon.ca/">Innovation Works</a></li>
                  <li><a href="http://www.bigbluebubble.com/">Big Blue Bubble</a></li>
                  <li><a href="http://www.rtraction.com/">rTraction</a></li>
                  <li><a href="https://www.londontourism.ca/">Tourism London</a></li>
                </ul>
			     </div>
           <div class="small-12 medium-4 ">
 								<ul>
                   <li><a href="http://www.museumlondon.ca/">Museum London</a></li>
                   <li><a href="https://www.fanshawec.ca/">Fanshawe College</a></li>
                   <li><a href="http://www.uwo.ca/">Western University</a></li>
                   <li><a href="http://www.artsproject.ca/">The Arts Project</a></li>
                   <li><a href="https://jrlma.ca/">Jack Richardson London Music Awards</a></li>
                   <li><a href="https://www.londonmusicoffice.com/">London Music Office</a></li>
                   <li><a href="http://www.sunfest.on.ca/">Sunfest</a></li>
                   <li><a href="http://homecounty.ca/">Home County</a></li>
                   <li><a href="https://www.allstage.ca/">Allstage</a></li>
                   <li><a href="https://apersondisguisedaspeople.bandcamp.com/">A Person Disguised As People</a></li>
                   <li><a href="https://londonfuse.ca/">London Fuse</a></li>
                 </ul>
 			     </div>
           <div class="small-12 medium-4 ">
 								<ul>
                   <li><a href="http://www.cbc.ca/news/canada/london">CBC London</a></li>
                   <li><a href="http://www.coventmarket.com/">Covent Garden Market</a></li>
                   <li><a href="https://www.budweisergardens.com/">Budweiser Gardens</a></li>
                 </ul>
 			     </div>
         </div>
		</div>
	</div>

	<?php include "includes/footer.php" ?>

	<script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
		<script src="js/home.js"></script>
</body>
</html>
