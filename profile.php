<?php
	require_once('admin/phpscripts/config.php');
	confirm_logged_in();

	$tbl = "tbl_users";
	$col = "user_id";
	$id = $_SESSION['user_id'];

	$result = getOne($tbl, $col, $id);
	$rowsa = mysqli_fetch_array($result);
	$company = "'{$rowsa['user_company']}'";

	 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Digital London</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

	<!-- Header -->
<?php include "includes/menu.php"; ?>
	<div class="hero hero--login">

	</div>

	<div class="container container-login">
		<?php include "includes/companymenu.php"; ?>
		<br>

		<?php
		$tbla = "tbl_company";
		$cola = "company_name";
		// $result = getOne($tbl, $col, $id);

		$compres = getSingle($tbla, $cola, $company);
		// while ($row = mysqli_fetch_array($compres)) {
		// 	echo "{$row['company_name']}";
		// }
		while($row = mysqli_fetch_array($compres)) {
			echo "<div class=\"grid-x container__company\">
						<div class=\"cell medium-4 \">
							<img src=\"img/{$row['company_image']}\" alt=\"{$row['company_name']}\">
						</div>

						<div class=\"cell medium-4 small-12 medium-offset-2 container__company__description\">

							<span class=\"grid-x\">
								<div class=\"cell small-6\">Company name:</div>
								<div class=\"cell small-6\">{$row['company_name']}</div>
							</span>

							<span class=\"grid-x\">
								<div class=\"cell small-6\">Adress:</div>
								<div class=\"cell small-6\">{$row['company_address']}</div>
							</span>

							<span class=\"grid-x\">
								<div class=\"cell small-6\">Phone:</div>
								<div class=\"cell small-6\">{$row['company_phone']}</div>
							</span>

							<span class=\"grid-x\">
								<div class=\"cell small-6\">Website:</div>
								<div class=\"cell small-6\">{$row['company_link']}</div>
							</span>";
			}
			$result2 = getOne($tbl, $col, $id);

			while($rows = mysqli_fetch_array($result2)) {
				echo "<span class=\"grid-x\">
					<div class=\"cell small-6\">Username:</div>
					<div class=\"cell small-6\">{$rows['user_name']}</div>
				</span>

				<span class=\"grid-x\">
					<div class=\"cell small-6\">ID Number:</div>
					<div class=\"cell small-6\">{$rows['user_id']}</div>
				</span>

				<span class=\"grid-x\">
					<div class=\"cell small-6\">Email:</div>
					<div class=\"cell small-6\">{$rows['user_email']}</div>
				</span>
			</div>
		</div>";
			}
		 ?>
	</div>

	<?php include "includes/footer.php" ?>

	<script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
</body>
</html>
