<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Digital London</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>

</head>
<body class="contactb">

	<!-- Header -->
<?php include "includes/menu.php"; ?>
	<div class="hero hero--contact">

	</div>

	<div class="container contact-con">
		<div class="cont">
		<div class="form">
			<h3 class="form__message">WE'D LOVE <span>HEAR FORM YOU</span> , JUST DROP US A <span>MESSAGE</span>  AND <span>WE'LL GET BACK</span>  AS SOON AS POSSIBLE.</h3>
		</div>

		<form >
		    <div class="grid-x">
		   		<div class="medium-5 cell form">
		          	<input type="text" placeholder="FULL NAME">
		          	<input type="text" placeholder="EMAIL">
		          	<input type="text" placeholder="SUBJECT">
		   		</div>

		   		<div class="medium-6 medium-offset-1 cell form">
	   			 	<textarea placeholder="MESSAGE"></textarea>

	   			 	<button class="button">SEND</button>
		   		</div>
		    </div>
		</form>


		<form class="form">
			<div>
				<h3 class="form__message--small" data-aos="fade-up"
     data-aos-anchor-placement="top-bottom">
					To know more about the life in London, the events and it's Tech side - SUBSCRIBE TO OUR NEWSLETTER.</h3>
			</div>

			<div class="input-group" data-aos="fade-up"
     data-aos-anchor-placement="top-bottom">
		  		<input class="input-group-field" type="text" placeholder="Enter your email address">
			   	<span class="button-inline input-group-label">
			   		<img class="send" src="img/send.png" alt="">
			   	</span>
			</div>
		</form>

</div>
	</div>

	<?php include "includes/footer.php" ?>

	<script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
		<script src="js/contact.js"></script>
</body>
</html>
