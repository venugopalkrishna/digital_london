<?php
  require_once('admin/phpscripts/config.php');
  $ip = $_SERVER['REMOTE_ADDR'];// if asked, you can make sure that the only place where you can access the info is IN THE OFFICE
  // echo $ip;
  if(isset($_POST['submit'])){
    // echo "works";
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $email = trim($_POST['email']);
    $company = trim($_POST['company']);
    if($username !== "" && $password !== "" && $email !== "" && $company !== ""){ // == not identical to
      $result = logIn($username, $password, $email, $company, $ip);
      $message = $result;
    }else{
      $message = "Please fill out the required (ALL) fields";
    }
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Digital London</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>

</head>
<body class="signinb">
	<!-- Header -->
  <?php include "includes/menu.php"; ?>

  <div class="hero hero--contact hero--signin">

  </div>

  <div class="container contact-con">
		<div class="cont">

		<div class="form">
			<h3 class="form__message">TO POST <span>ADS, EVENTS / JOBS OR CREATE AN ACCOUNT</span> , SIGN IN WITH THE REGISTERED DETAILS.</h3>
		</div>
    <p><?php if(!empty($message)){ echo $message; } ?></p>
    <form class="form" action="admin_login.php" method="post">
		    <div class="grid-x align-center">
		   		<div class="medium-5  cell">
		          	<input type="text" name="username" value="" placeholder="MEMBERSHIP NUMBER / USERNAME">
		          	<input type="text" name="company" value="" placeholder="COMPANY NAME">
              </div>
                <div class="medium-5  cell medium-offset-1">
                      <input type="password" name="password" value="" placeholder="PASSWORD">
                      <input type="email" name="email" value="" placeholder="EMAIL">
                      <h3 class="form__message">Forgot your login details? Click <span style="border-bottom: 1px solid">here</span></h3>
                </div>
              </div>
              <input type="submit" name="submit" class="button" value="SIGN IN">
              </form>
              <h3 class="form__message"  data-aos="fade-up"
              data-aos-anchor-placement="top-bottom" ><span><a href="admin/admin_register.php">CLICK HERE</a>	</span> IF YOU DON'T HAVE AN ACCOUNT.</h3>

              </div>
              </div>

              <?php include "includes/footer.php" ?>
              <script src="js/vendor/jquery.js"></script>
              <script src="js/vendor/what-input.js"></script>
              <script src="js/vendor/foundation.js"></script>
              <script src="js/app.js"></script>
              <!-- <script src="admin/js/signin.js"></script> -->
              <script type="text/javascript" src="js/signinaos.js">

              </script>
              <script type="text/javascript">


              var scrollitem = document.querySelector(".container").offsetTop;
              var hero = document.querySelector(".hero");

              window.onscroll = function() {
                console.log("works?");
                if (window.pageYOffset > 0) {
               var opac = (window.pageYOffset / scrollitem);
                  console.log(opac);
                hero.style.background = "linear-gradient(rgba(255, 255, 255, " + opac + "), rgba(255, 255, 255, " + opac + ")), url('img/banner_signin.jpg') no-repeat fixed";
                }
              }

              </script>



</body>
</html>
