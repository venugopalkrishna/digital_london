<?php
require_once('admin/phpscripts/config.php');
confirm_logged_in();

$tbl = "tbl_users";
$col = "user_id";
$id = $_SESSION['user_id'];

$result = getOne($tbl, $col, $id);
$row = mysqli_fetch_array($result);
$company = $row['user_company'];
// $company = "Northern";
$tbla = "job";
$resultcomp = pastevent($company, $tbla);
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Digital London</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

	<!-- Header -->
<?php include "includes/menu.php"; ?>
	<div class="hero hero--login">

	</div>

	<div class="container container-login">
		<div class="form">
			<br>
      <?php include "includes/companymenu.php"; ?>

		</div>
		<br>

		<div class="grid-x container__company">
			<?php
			$row1 = mysqli_fetch_array($resultcomp);
			echo "<div class=\"cell medium-4 \">
							<img src=\"img/{$row1['company_image']}\" alt=\"{$row1['company_name']}\">
						</div>";

      while($row = mysqli_fetch_array($resultcomp)) {
        echo "<div class=\"cell medium-4 small-12 medium-offset-2 container__company__description\">
          <span class=\"grid-x\">
            <div class=\"cell small-6\">{$row['job_position']}</div>
          </span>
          <span class=\"grid-x\">
            <div class=\"cell small-6\">{$row['job_date']}</div>
          </span>
          <span class=\"grid-x\">
            <div class=\"cell small-6\">{$row['job_duration']}</div>
          </span>
        </div>";

			}
       ?>
		</div>

	</div>

  <?php include "includes/footer.php" ?>

	<script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
</body>
</html>
