<?php include('admin/phpscripts/config.php');
$tbl = "tbl_index";

$getValues = getAll($tbl);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Digital London</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
</head>
<body style="background: url('img/banner_home.gif') no-repeat fixed; background-size: 100vw;" class="home">

	<!-- Header -->
	<?php include "includes/menu.php"; ?>

	<div class="hero">

	</div>

	<div class="data__company">
		<!--stats div-->
		<div class="data grid-x">
			<div class="data__item cell small-6 medium-3 tc">
				<h3 id="tech">0</h3>
				<span>Tech Companies</span>
			</div>

			<div class="data__item cell small-6 medium-3 eo">
				<h3 id="emplo">0</h3>
				<span>Employment Opportunities</span>
			</div>

			<div class="data__item cell small-6 medium-3">
				<h3 id="empl">0</h3>
				<span>Employment</span>
			</div>

			<div class="data__item cell small-6 medium-3">
				<h3 id="enrol">0</h3>
				<span>Students Enrolled</span>
			</div>

		</div>
		<div class="description">

			<div class="container">
				<div class="grid-x">

					<!--video div-->

					<div class="small-12 medium-6 ">
							<div data-aos="fade-right">
								<!-- <video src="videos/dl_long.mp4" controls class="cell small-12 medium-6" >
								</video> -->
								<?php

								while($row = mysqli_fetch_array($getValues)){
																					echo "<video src=\"videos/{$row['index_video']}\" controls class=\"cell small-12 medium-6\" >
																					</video>
					</div>
					</div>
                                  <p class=\"cell small-12 medium-6 paragraph\" data-aos=\"fade-left\">{$row['index_content']}<br><span><a style=\"color: inherit;\" href=\"references.php\">Video References</a></span></p>
																		<p class=\"paragraph__alt\">{$row['index_content2']}
																		</p>"
																	;
                                }
                              ?>
					<!-- <p class="cell small-12 medium-6 paragraph" data-aos="fade-left">

Welcome to Digital London, Canada. Home to a thriving tech industry and digital creative sector, with a diverse range of careers and opportunities. Shaping the world of the future together.					</p>

				<p class="paragraph__alt">
London brings an excellent quality of life, a good family oriented community, strong cultural events, and opportunities to shape the city while contributing to global Digital Creative and Tech conversations and innovations.
				<br><br>
London also has a supportive and collaborative network of agencies, educators and
professionals who are collectively pushing for a brighter future with a digital landscape.

				</p> -->
</div>
			</div>
		</div>
	</div>

	<div class="posts">
		<div class="container">
			<div class="posts__item">
				<img src="img/digitree.jpg" alt="">
			</div>
			<div class="posts__item">
				<img src="img/Post .jpg" alt="">
			</div>
			<div class="posts__item">
				<img src="img/poster-1.jpg" alt="">
			</div>
			<div class="posts__item">
				<img src="img/poster-2.jpg" alt="">
			</div>
			<div class="posts__item">
				<img src="img/digital-headquarters.jpg" alt="">
			</div>
		</div>
	</div>

	<?php include "includes/footer.php" ?>

	<script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
		<script src="js/home.js"></script>
</body>
</html>
