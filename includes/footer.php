<footer class="footer footer--onlymobile">
  <div class="container-footer">
    <ul>
      <li><a href="">Digital London</a></li>
      <li><a href="">Careers</a></li>
      <li><a href="">Privacy Policy</a></li>
      <li><a href="">Disclaimer</a></li>
      <li><a href="">Copyright</a></li>
      <li><a href="">Sitemap</a></li>
    </ul>

    <div class="social">
      <img src="img/twitter.svg" alt="Twitter">
      <img src="img/facebook.svg" alt="facebook">
      <img src="img/instagram.svg" alt="Twitter">
    </div>
  </div>
</footer>
