<header class="header">


  <div class="title-bar" data-responsive-toggle="example-animated-menu" data-hide-for="medium">
      <button class="menu-icon" type="button" data-toggle></button>
    <img src="img/logo.png" class="menu__logo--small">
  </div>

  <div class="top-bar" id="example-animated-menu">

    <div class="top-bar-left">
        <div>
        <img src="img/logo.png" class="menu__logo">
        </div>

      </div>

      <div class="top-bar-right">
      <ul class="dropdown menu" data-dropdown-menu>

        <li><a href="index">Home</a></li>
        <li><a href="woods">Behind the woods</a></li>
        <li><a href="testimonials">Testimonials</a></li>
        <li><a href="events">Events</a></li>
        <li><a href="jobs">Jobs</a></li>
        <li><a href="contact">Contact</a></li>
        <li><a href="admin_login">Sign In</a></li>
        </ul>
      </div>
  </div>

</header>
