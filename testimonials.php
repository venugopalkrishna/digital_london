<?php include('admin/phpscripts/config.php');
$tbl = "tbl_testimonies";
$count = 0;
$getValues = getAll($tbl);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Digital London</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>

</head>
<body class="testimonialsb">

	<!-- Header -->
<?php include "includes/menu.php"; ?>
	<div class="hero hero--testimonials">
<!-- <h1 class="heading">TESTIMONIALS</h1> -->
	</div>
	<div style="height:5%; background-color: #11434C"></div>


			<!-- <div class="testimonials__image" data-aos="fade-right"> -->
				<!-- <img src="img/testimonials_1.jpg" alt=""> -->
				<?php
				while($row = mysqli_fetch_array($getValues)){
					if($count === 0){
																	echo "
																	<div class=\"testimonials\">
																		<div class=\"testimonials__item\">
																		<div class=\"testimonials__image\" data-aos=\"fade-right\">
																	<video src=\"videos/{$row['test_video']}\" controls poster=\"img/{$row['test_image']}\" >
																	</video>
																		</div>
																	<div class=\"testimonials__info__container\" data-aos=\"fade-right\">

																			<div class=\"testimonials__info\">

																				<div class=\"testimonials__person\">
																				{$row['test_name']}
																					<h3>{$row['test_company']}</h3>
																	{$row['test_title']}
																		</div>
																		<div class=\"testimonials__description\">
																		{$row['test_description']}
													</div>
													</div>
												</div>
											</div>";
											$count++;

																}
								else{
									echo "
									<div class=\"testimonials__item testimonials__item--two\">
									<div class=\"testimonials__info__container\" data-aos=\"fade-left\">
								<div class=\"testimonials__info testimonials__info--left\">
									<div class=\"testimonials__person\">
									{$row['test_name']}
								<h3>{$row['test_company']}</h3>
								{$row['test_title']}
								</div>
								<div class=\"testimonials__description\">
								{$row['test_description']}
								</div>
								</div>
								</div>
								<div class=\"testimonials__image\" data-aos=\"fade-left\">
									<video src=\"videos/{$row['test_video']}\" controls poster=\"img/{$row['test_image']}\">

									</video>
								</div>

									</div>";
$count = 0;
								}
							}

				?>

				<?php include "includes/footer.php" ?>

	<script src="js/testimonials.js"></script>


	<script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
</body>
</html>
