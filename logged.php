<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Digital London</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>

</head>
<body class="signinb">
	<!-- Header -->
	<header class="header">


		<div class="title-bar" data-responsive-toggle="example-animated-menu" data-hide-for="medium">
		  	<button class="menu-icon" type="button" data-toggle></button>
			<img src="img/logo.png" class="menu__logo--small">
		</div>

		<div class="top-bar" id="example-animated-menu">

			<div class="top-bar-left">
		    	<div>
				 	<img src="img/logo.png" class="menu__logo">
		    	</div>

		  	</div>

		  	<div class="top-bar-right">
				<ul class="dropdown menu" data-dropdown-menu>

					<li><a href="index.php">Home</a></li>
					<li><a href="testimonials.html">Testimonials</a></li>
					<li><a href="events.html">Events</a></li>
					<li><a href="jobs.html">Jobs</a></li>
					<li><a href="contact.html">Contact</a></li>
					<li><a href="#">Sign Out</a></li>
			    </ul>
		  	</div>
		</div>

	</header>

	<div class="hero hero--login">

	</div>

	<div class="container container-login">
		<div class="form">
			<br>
			<div class="grid-x align-between">

				<h5 class="cell medium-3">COMPANY PROFILE</h5>


				<h5 class="cell medium-3">PAST EVENTS</h5>


				<h5 class="cell medium-3">PAST JOBS</h5>


				<h5 class="cell medium-3 text-right">NEW JOBS/EVENTS</h5>

			</div>

		</div>
		<br>

		<form class="form-simple">

	  		<div class="grid-container">
		    <div class="grid-x grid-padding-x">
		    	<div class="grid-x medium-10 cell">

			    	<fieldset class="medium-5  cell">
					    <input id="job" type="radio"><label for="job">Job</label>
					    <br>
					    <input id="event" type="radio"><label for="event">Event</label>
				  	</fieldset>

			  		<fieldset class="medium-5 medium-offset-1 cell cell">
					    <input id="job" type="checkbox"><label for="job">Register</label>
					    <br>
					    <input id="event" type="checkbox"><label for="event">Contact</label>
				  	</fieldset>
			      	<div class="medium-5 cell">
			      		<label for="">Company Name</label>
			          	<input type="text" placeholder="">
			      	</div>

			      	<div class="medium-5 medium-offset-1 cell">
			      		<label for="">Date</label>
			          	<input type="text" placeholder="">
			      	</div>

			      	<div class="medium-5 cell">
			      		<label for="">Title</label>
			          	<input type="text" placeholder="">
			      	</div>

			      	<div class="medium-5 medium-offset-1 cell">
			      		<label for="">Duration of post</label>
			          	<input type="date" placeholder="">
			      	</div>

			      	<div class="medium-5 cell">
			      		<label for="">Image, if not the logo</label>
			          	<input type="text" placeholder="">
			      	</div>

			      	<div class="medium-5 medium-offset-1 cell">
			      		<label for="">Image, if not the logo</label>
			          	<input type="text" placeholder="">
			      	</div>

			      	<div class="medium-11 cell">
			      		<label for="">Description</label>
			          	<textarea name="" id="" cols="30" rows="10"></textarea>
			      	</div>
		    	</div>

		    	<div class="form medium-2">
		    		<button class="button">Post</button>
		    		Confirmation will be sent to registered E-mail adress
		    	</div>

		  	</div>
		 	</div>
		</form>



	</div>

	<footer class="footer">
		<div class="container-footer">
			<ul>
				<li><a href="">Digital London</a></li>
				<li><a href="">Careers</a></li>
				<li><a href="">Privacy Policy</a></li>
				<li><a href="">Disclaimer</a></li>
				<li><a href="">Copyright</a></li>
				<li><a href="">Sitemap</a></li>
				<li><a href="https://www.ledc.com/">Ledc</a></li>
			</ul>

			<div class="social">
				<img src="img/twitter.svg" alt="Twitter">
				<img src="img/facebook.svg" alt="facebook">
				<img src="img/instagram.svg" alt="Twitter">
			</div>
		</div>
	</footer>


	<script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
</body>
</html>
