<?php
	require_once('admin/phpscripts/config.php');
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Digital London</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/events.css">
</head>
<body>

	<!-- Header -->
<?php include "includes/menu.php" ?>
	<div style="background: url('img/banner_events.png') no-repeat;" class="hero hero--events" id="hero--events">

	</div>

	<div class="container container-login">
		<div class="form">
			<br>
			<!-- <div class="grid-x align-between company__tags">

				<a href="profile.html" class="cell medium-3">COMPANY PROFILE</a>


				<a href="past_events.html" class="cell medium-3">PAST EVENTS</a>


				<a href="past_jobs.html" class="cell medium-3">PAST JOBS</a>


				<a href="#" class="cell medium-3 ">NEW JOBS/EVENTS</a>

			</div> -->

		</div>
		<br>

		<div class="grid-x container__company events">

			<?php
			$tbl = "tbl_event";
			$col = "event_type";
			$type = "'tech'";
			$count = 0;
			$getinfo = getSingle($tbl, $col, $type);
	      while($row = mysqli_fetch_array($getinfo)) {
					if($count === 0){
						echo "<div class=\"cell medium-4 eventDiv\">
							<img src=\"img/{$row['event_img']}\" alt=\"{$row['event_name']}\">
							<h1>{$row['event_name']} {$row['event_date']} {$row['event_location']}</h1>
							<p id=\"title_event1\">{$row['event_description']}</p><br>
			       <a href=\"{$row['event_link']}\"><button class=\"button_jobs\">BUY TICKETS NOW!</button></a>
						</div>";
						$count++;

					}else{
						echo "<div class=\"cell medium-4 eventDiv companyborder\">
							<img src=\"img/{$row['event_img']}\" alt=\"{$row['event_name']}\">
							<h1>{$row['event_name']} {$row['event_date']} {$row['event_location']}</h1>
							<p id=\"title_event1\">{$row['event_description']}</p><br>
						 <a href=\"{$row['event_link']}\"><button class=\"button_jobs\">BUY TICKETS NOW!</button></a>
						</div>";
						$count = 0;

					}
				}
	     ?>


		</div>

	</div>

	<?php include "includes/footer.php" ?>

	<script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>

		<script type="text/javascript">

		var scrollitem = document.querySelector(".events").offsetTop;
		var hero = document.querySelector(".hero");

		window.onscroll = function() {
		  if (window.pageYOffset > 0) {
		 var opac = (window.pageYOffset / scrollitem);
		    console.log(opac);
		  hero.style.background = "linear-gradient(rgba(255, 255, 255, " + opac + "), rgba(255, 255, 255, " + opac + ")), url('img/banner_events.png') no-repeat fixed";
		  }
		}

		</script>
</body>
</html>
