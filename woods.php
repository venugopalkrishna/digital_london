<?php
	require_once('admin/phpscripts/config.php');
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Digital London</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
</head>
<body class="testimonialsb">

	<!-- Header -->
<?php include "includes/menu.php"; ?>
	<div class="hero hero--woods" id="hero--woods">

	</div>
	<div style="height:5%; background-color: #11434C"></div>

		<?php
		$tbl = "tbl_woods";
		$count = 0;
		$getValues = getAll($tbl);

		while($row = mysqli_fetch_array($getValues)){
			if($count === 0){
				echo "<div class=\"testimonials\">
				<div class=\"testimonials__item\">
					<div class=\"testimonials__image\" data-aos=\"fade-right\">";
					if($row['woods_video'] == "NULL") {
							echo "<img src=\"img/{$row['woods_image']}\" alt=\"{$row['woods_image']}\">";
					}else{
						echo "<a href=\"{$row['woods_video']}\"><img src=\"img/{$row['woods_image']}\" alt=\"{$row['woods_image']}\"></a>";
					}
					echo "</div>

							<div class=\"testimonials__info__container\" data-aos=\"fade-right\">";
					if($row['woods_title'] == "NULL" && $row['woods_name'] == "NULL" && $row['woods_location'] == "NULL"){
						echo "<div class=\"hidden\"></div>";
					}else{
						echo "<div class=\"testimonials__person\">";
						if($row['woods_title'] == "NULL"){
							echo "{$row['woods_name']}
							<h3>{$row['woods_location']}</h3>";
						}	else {
							echo "{$row['woods_title']}
							<h3>{$row['woods_name']}</h3>
							{$row['woods_location']}";
						}
						echo "</div>";
					}
					echo "<div class=\"testimonials__info\">

									<div class=\"testimonials__description\">
									{$row['woods_description']}
									</div>
								</div>
							</div>
						</div>
						</div>
						</div>";
						$count++;
					}else{
						echo "<div class=\"testimonials__item testimonials__item--two\">

						<div class=\"testimonials__info__container\" data-aos=\"fade-left\">


								<div class=\"testimonials__info testimonials__info--left\">";

								if($row['woods_title'] == NULL && $row['woods_name'] == NULL && $row['woods_location'] == NULL){
									echo "<div class=\"hidden\"></div>";
								}else{
									echo "
									<div class=\"testimonials__person\">";
									if($row['woods_title'] == NULL){
										echo "{$row['woods_name']}
										<h3>{$row['woods_location']}</h3>";
									}else{
										echo "{$row['woods_title']}
										<h3>{$row['woods_name']}</h3>
										{$row['woods_location']}";
									}
									echo "</div>";
								}
								echo "<div class=\"testimonials__description\">
									{$row['woods_description']}
								 </div>
								</div>

							</div>

							<div class=\"testimonials__image\" data-aos=\"fade-left\">";
							if($row['woods_video'] == "NULL") {
									echo "<img src=\"img/{$row['woods_image']}\" alt=\"{$row['woods_image']}\">";
							}else{
								echo "<a href=\"{$row['woods_video']}\"><img src=\"img/{$row['woods_image']}\" alt=\"{$row['woods_image']}\"></a>";
							}
							echo "</div>
						</div>";
								$count = 0;
						}
					}

		?>



		<div class="cell medium-12">
			<h1 id="divider">A city of green, a city of fun!</h1>
		</div>



		<div class="grid-x container__company">

			<?php
			$tbl = "tbl_event";
			$col = "event_type";
			$type = "'tour'";
			$count = 0;
			$getinfo = getSingle($tbl, $col, $type);
	      while($row = mysqli_fetch_array($getinfo)) {
					if($count === 0){
						echo "<div class=\"cell medium-4 eventDiv\">
							<img src=\"img/{$row['event_img']}\" alt=\"{$row['event_name']}\">
							<h1>{$row['event_name']} - {$row['event_date']} - {$row['event_location']}</h1>
							<p id=\"title_event1\">{$row['event_description']}</p><br>
			       <a href=\"{$row['event_link']}\"><button class=\"button_jobs\">BUY TICKETS NOW!</button></a>
						</div>";
						$count++;

					}else{
						echo "<div class=\"cell medium-4 eventDiv companyborder\">
							<img src=\"img/{$row['event_img']}\" alt=\"{$row['event_name']}\">
							<h1>{$row['event_name']} {$row['event_date']} {$row['event_location']}</h1>
							<p id=\"title_event1\">{$row['event_description']}</p><br>
						 <a href=\"{$row['event_link']}\"><button class=\"button_jobs\">BUY TICKETS NOW!</button></a>
						</div>";
						$count = 0;

					}
				}
	     ?>

		</div>


	</div>
<?php include "includes/footer.php" ?>
	<script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
		<script src="js/woods.js"></script>
		<script type="text/javascript">
		console.log("conected");

	var scrollitem = document.querySelector(".testimonials").offsetTop;
	var hero = document.querySelector(".hero");

	window.onscroll = function() {
	  console.log("works?");
	  if (window.pageYOffset > 0) {
	 var opac = (window.pageYOffset / scrollitem);
	    console.log(opac);
	  hero.style.background = "linear-gradient(rgba(255, 255, 255, " + opac + "), rgba(255, 255, 255, " + opac + ")), url('img/banner_woods.jpg') no-repeat fixed";
	  }
	}

		</script>

</body>
</html>
