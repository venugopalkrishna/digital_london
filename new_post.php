<?php
require_once('admin/phpscripts/config.php');
// confirm_logged_in();
$tbl1 = "tbl_users";
$col1 = "user_id";
$id1 = $_SESSION['user_id'];

$result = getOne($tbl1, $col1, $id1);

if(isset($_POST['submit'])){
	$posttype = $_POST['posttype'];
	$company = $_POST['company'];
	$title = $_POST['title'];
	$date = $_POST['date'];
	$duration = $_POST['duration'];
	$location = $_POST['location'];
	$img = $_FILES['image'];
	$linksys = $_POST['linksys'];
	$description = $_POST['description'];
	$typepo = $_POST['typepo'];
	if(empty($posttype)){
		$message = "Please select a posting type";
	}else{
		$result = joborevent($posttype, $company, $title, $date, $duration, $location, $img, $description, $linksys, $typepo);
		$message = $result;
	}

}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Digital London</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

	<!-- Header -->
<?php include "includes/menu.php"; ?>
	<div class="hero hero--login">

	</div>

	<div class="container container-login">
		<?php include "includes/companymenu.php"; ?>
		<br>

		<form class="form-simple" action="new_post.php" method="post" enctype="multipart/form-data">
			<p><?php if (!empty($message)){echo $message; } ?></p>
	  		<div class="grid-container">
		    <div class="grid-x grid-padding-x">
		    	<div class="grid-x medium-10 cell">
			    	<fieldset class="medium-5  cell">
					    <input id="job" type="radio" name="posttype" value="job"><label for="job">Job</label>
					    <br>
					    <input id="event" type="radio" name="posttype" value="event"><label for="event">Event</label>
				  	</fieldset>

			  		<fieldset class="medium-5 medium-offset-1 cell cell">
					    <input id="job" type="checkbox"><label for="job">Register</label>
					    <br>
					    <input id="event" type="checkbox"><label for="event">Contact</label>
				  	</fieldset>
			      	<div class="medium-5 cell">
			      		<label for="">Company Name</label>
								<?php
								while($row = mysqli_fetch_array($result)) {
									echo "<input type=\"text\" name=\"company\" placeholder=\"\" value=\"{$row['user_company']}\" disabled>";
									echo "<input hidden name=\"company\" placeholder=\"\" value=\"{$row['user_company']}\">";
								}
								 ?>
								 <!-- <input hidden name="company" placeholder="" value="Diply"> -->
			      	</div>

			      	<div class="medium-5 medium-offset-1 cell">
			      		<label for="">Date</label>
			          	<input type="text" name="date" placeholder="" value="">
			      	</div>

			      	<div class="medium-5 cell">
			      		<label for="">Title</label>
			          	<input type="text" name="title" placeholder="" value="">
			      	</div>

			      	<div class="medium-5 medium-offset-1 cell">
			      		<label for="">Duration of post</label>
			          	<input type="date" name="duration" placeholder="" value="">
			      	</div>

							<div class="medium-5 cell">
			      		<label for="">Image, if not the logo</label>
			          	<input type="file" name="image" placeholder="" value="">
			      	</div>

			      	<div class="medium-5 medium-offset-1 cell">
			      		<label for="">Location (for events)</label>
			          	<input type="text" name="location" placeholder="" value="">
			      	</div>

							<div class="medium-5 cell">
			      		<label for="">Link</label>
			          	<input type="text" name="linksys" placeholder="" value="">
			      	</div>

							<div class="medium-5 medium-offset-1 cell">
			      		<label for="">Type</label>
			          	<select class="" name="typepo">
			          		<option value="tech">Tech related</option>
										<option value="tour">City related</option>
			          	</select>
			      	</div>

			      	<div class="medium-11 cell">
			      		<label for="">Description</label>
			          	<textarea name="description" id="" cols="30" rows="10"></textarea>
			      	</div>

		    	</div>

		    	<div class="form medium-2">
		    		<input class="button" type="submit" name="submit" value="Post">
		    		Confirmation will be sent to registered E-mail adress
		    	</div>

		  	</div>
		 	</div>
		</form>



	</div>

	<?php include "includes/footer.php" ?>

	<script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
</body>
</html>
